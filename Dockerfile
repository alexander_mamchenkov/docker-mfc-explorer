FROM mamchenkov/mfcoind-ubuntu

LABEL org.label-schema.schema-version="1.0" \
    org.label-schema.name="MFC Block Explorer" \
    org.label-schema.vendor="Alexander Mamchenkov" \
    org.label-schema.livence="MIT" \
    org.label-schema.build-data="20180702"

MAINTAINER Alexander Mamchenkov <mamtchenkov@gmail.com>

COPY litesight.tar.gz /opt/litesight.tar.gz
COPY nvm.tar.gz /root/nvm.tar.gz

RUN cd /root \
    && tar -xzf nvm.tar.gz \
    && rm -rf nvm.tar.gz \
    && cd /opt \
    && tar -xzf litesight.tar.gz \
    && rm -rf litesight.tar.gz

COPY entrypoint.sh /usr/local/bin/entrypoint.sh

EXPOSE 80

CMD ["/usr/local/bin/entrypoint.sh"]
