#!/bin/bash

cd /root
if [ ! -d .MFCoin ];
then
    apt-get install wget
    wget https://www.dropbox.com/s/f6a3465zudvltz9/latest.tar.gz?dl=1 -O latest.tar.gz
    tar -xzf latest.tar.gz
    rm latest.tar.gz
    echo "rpcuser=MFCoinrpc" >> /root/.MFCoin/MFCoin.conf
    echo "rpcpassword=MFCoinrpcPass" >> /root/.MFCoin/MFCoin.conf
fi

MFCoind -daemon -txindex

echo "Sleeping for 2 minutes to give MFCoind time to connect and sync"
sleep 120

# Clear Litesight cache to force resync from MFCoind
rm -rf ~/.reddsight_MFCoin

cd /opt/Litesight
export NODE_ENV='production'
export INSIGHT_NETWORK='livenet'
/root/.nvm/v0.10.25/bin/npm start
