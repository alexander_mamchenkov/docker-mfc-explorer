# docker-mfc-explorer

Litesight block explorer docker image for MFC

Allow it some time to sync after start.

Exposed ports:

* 80: Web interface of block explorer

DockerHub: https://hub.docker.com/r/mamchenkov/mfc-explorer/
